package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"net/http"
	"runtime"
	"sync"
	"testing"
	"time"
)

var quota = runtime.NumCPU()

func parser() {
	runtime.GOMAXPROCS(quota)
	var myClient = &http.Client{Timeout: 10 * time.Second}
	r, err := myClient.Get("https://api.huobi.pro/market/tickers")
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	var c Raw
	json.NewDecoder(r.Body).Decode(&c)

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	quoteCh := make(chan struct{}, quota)
	wg := &sync.WaitGroup{}
	wg.Add(len(c.Data))
	for _, coin := range c.Data {
		quoteCh <- struct{}{}
		go func(c Coin, quote chan struct{}) {
			client.Set(c.Symbol, fmt.Sprintf("%f", c.Close), 0)
			wg.Done()
			<-quote
		}(coin, quoteCh)
	}
	wg.Wait()
	//for _, coin := range c.Data {
	//	fmt.Println(client.Get(coin.Symbol).Result())
	//}
}

func main() {
	parser()
}
func BenchmarkParser(b *testing.B) {
	for i := 0; i < b.N; i++ {
		parser()
	}
}
