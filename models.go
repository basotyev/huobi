package main

type Coin struct {
	Symbol string  `json:"symbol"`
	Close  float64 `json:"close"`
}

type Raw struct {
	Data []Coin `json:"data"`
}
